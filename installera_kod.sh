#!/usr/bin/env bash
PROGRAM="$HOME/Program"
KOD="$(cd $(dirname $0)/Kod && pwd)"
if [[ ! -d "$PROGRAM" ]] ; then
	rm -f "$PROGRAM"
	mkdir "$PROGRAM"
fi
for prog in bsod caffeine illuminati ison lock mknonsense uncertain vibootcfg ; do
	ln -fs "$KOD/$prog" "$PROGRAM"
done
for imperativ in dice sag say säg 說吧 说吧 ; do
	ln -fs "$KOD/speech" "$PROGRAM/$imperativ"
done
cd "$KOD"
for c in piano typewrite ; do
	gcc -Wall $c.c @$c.flags -o "$PROGRAM/$c"
done
for haskell in collatz collatzall sus ; do
	ghc -Wall -dynamic $haskell.hs -o "$PROGRAM/$haskell"
done
for prog in ludvigetty startnyckel-hook tpcbbt ; do
	sudo cp -f $prog /usr/local/bin/
done

#!/usr/bin/env bash
cd ../grub
./grub-mkstandalone \
        --grub-mkimage=./grub-mkimage \
        -O i386-coreboot \
        -o ../coreboot/grub2.elf \
        -d grub-core \
        --fonts= \
        --themes= \
        --locales= \
        --modules="cbfs" \
        /boot/grub/grub.cfg=../coreboot/memdisk.cfg \
        /boot/grub/layouts/se.gkb=../coreboot/se.gkb

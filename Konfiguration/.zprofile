export PATH="$HOME/Program:$PATH:."

if [[ $XDG_VTNR -eq 1 && ! -v $DISPLAY ]]; then
	exec startx >/dev/null 2>&1
fi

# Inställningar
setopt always_to_end
setopt auto_cd
setopt auto_pushd
setopt cdable_vars
unsetopt check_jobs
setopt combining_chars
setopt complete_in_word
setopt extended_glob
unsetopt flow_control
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_verify
unsetopt hup
setopt interactive_comments
setopt long_list_jobs
setopt prompt_subst
setopt pushd_minus
setopt pushd_to_home
setopt share_history

# Läs in saker
autoload -Uz compinit
compinit
zmodload zsh/complist
source /usr/share/doc/pkgfile/command-not-found.zsh

# Frys terminalen
ttyctl -f

# Definiera widgetar
autoload -Uz up-line-or-beginning-search
zle -N up-line-or-beginning-search

autoload -Uz down-line-or-beginning-search
zle -N down-line-or-beginning-search

function _intelligent_returtangent {
	case $BUFFER in
		\$?*)
			BUFFER="echo $BUFFER"
			zle accept-line
			;;
		'')
			echo
			if [[ "$(ls -1U | wc -l)" -gt "$((LINES - 2))" ]] ; then
				echo "För många objekt för att visa."
			else
				ls -FhoN --color=auto --group-directories-first
			fi
			zle reset-prompt
			;;
		*)
			zle accept-line
			;;
	esac
}
zle -N _intelligent_returtangent

# Tangentkommandon
bindkey "$terminfo[kcuu1]" up-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[OA" up-line-or-beginning-search
bindkey "$terminfo[kcud1]" down-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
bindkey "^[OB" down-line-or-beginning-search
bindkey "$terminfo[cr]" _intelligent_returtangent
bindkey "^M" _intelligent_returtangent

# Automatisk komplettering
zstyle ':completion:*' cache-path "$HOME/.cache/zsh"
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}" 'ma=37;45'
#zstyle ':completion:*' list-dirs-first true
zstyle ':completion:*' menu select=2
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' rehash true
zstyle ':completion:*' use-cache true

# Historik
HISTFILE="$HOME/.zshhist"
HISTSIZE=10000
SAVEHIST=10000

# Prompten
function _kort_wd {
	# Kopierad från Zim. TODO: lista ut vad som händer här
	local wd="${${1:-$PWD}/#$HOME/~}"
	if [[ "$wd" != '~' ]] ; then
		wd="${${${${(@j:/:M)${(@s:/:)wd}##.#?}:h}%/}//\%/%%}/${${wd:t}//\%/%%}"
	fi
	echo $wd
}
PROMPT='%F{magenta}%B%(?.:).%?)%f%b %F{cyan}%B$(_kort_wd)%f%b %F{white}%B»%f%b '

# Funktioner och alias
alias sd='sudo ' # Mellanslaget krävs för att alias ska funka med sudo
alias su='sudo -i'

alias e='$EDITOR'
alias se='sudo $EDITOR'

alias 1='cd -1'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'
alias d='dirs -v'
alias p='pwd'

alias j='jobs -l'
alias jr='jobs -lr'
alias js='jobs -ls'

alias k='kill'
alias c='kill -CONT'
alias s='kill -TSTP'

alias pk='pikaur'
alias pkc='pikaur -Scc'
alias pki='pikaur -S'
alias pkid='pikaur -S --asdeps'
alias pks='pikaur -Ss'
alias pkr='pikaur -Rsc'
alias pku='pikaur -Syu'
alias pkuu='pikaur -Syu --devel --needed'

alias pf='pkgfile'
alias pfl='pkgfile -l'

alias g='git'
alias ga='git add'
alias gaa='git add --all'
alias gc='git clone --recursive'
alias gm='git commit -vm'
alias gf='git fetch'
alias gl='git pull'
alias gp='git push'
alias gr='git rm'
alias gs='git status'

function um {
	udisksctl mount --block-device "/dev/disk/by-label/$1"
}
function uu {
	udisksctl unmount --block-device "/dev/disk/by-label/$1"
}

alias hib='systemctl hibernate'
alias off='systemctl poweroff'
alias reb='systemctl reboot'
alias ssp='systemctl suspend'

alias ls='ls --color=auto'
alias l='ls -FhoN --group-directories-first'
alias la='ls -AFhoN --group-directories-first'
alias ldot='ls -dFhoN --group-directories-first .*'

function path {
	if [[ -v 1 ]] ; then
		export PATH="$1"
	else
		echo "$PATH"
	fi
}
alias wh='where'            

alias cp='cp -fr'
alias rm='rm -r'

alias lo='locate -b'
alias udb='updatedb'

alias grep='grep --color=auto -n'
alias -g G='| grep'
alias -g L='| less'

alias cmd='wine cmd'
alias f='file'
alias hex='tweak -l'
alias hist='fc -il 1 | less +G'
alias manh='man -H'
alias p0x='ping -a 0x4c.se'
function t {
	case $TERM in
		alacritty*|xterm*)
			echo -en "\e]0;$@\a" ;;
		*)
			return 1 ;;
	esac
}

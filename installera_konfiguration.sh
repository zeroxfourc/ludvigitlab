#!/usr/bin/env bash
KONF="$(cd $(dirname $0)/Konfiguration && pwd)"
for dir in \
	.config/alacritty \
	.config/dunst \
	.config/htop \
	.config/i3 \
	.config/i3status \
	.config/rofi \
	.dte
do
	if [[ ! -d "$HOME/$dir" ]] ; then
		rm -f "$HOME/$dir"
		mkdir -p "$HOME/$dir"
	fi
done
for file in \
	.config/alacritty/alacritty.yml \
	.config/dunst/dunstrc \
	.config/htop/htoprc \
	.config/i3/config \
	.config/i3status/config \
	.config/rofi/config \
	.config/rofi/dmenuaktigt.rasi \
	.dte/rc \
	.xinitrc \
	.zprofile \
	.zshenv \
	.zshrc
do
	ln -fs "$KONF/$file" "$HOME/$file"
done
for dir in \
	etc/default \
	etc/fonts/conf.d \
	etc/pacman.d/hooks \
	etc/systemd/logind.conf.d \
	etc/systemd/system/getty@.service.d \
	etc/systemd/timesync.conf.d \
	etc/tmpfiles.d \
	etc/udev/hwdb.d \
	etc/X11/xorg.conf.d
do
	if [[ ! -d "/$dir" ]] ; then
		sudo rm -f "/$dir"
		sudo mkdir -p "/$dir"
	fi
done
for file in \
	etc/default/tpcbbt \
	etc/fonts/conf.d/99-aaaaaa.conf \
	etc/issue \
	etc/locale.conf \
	etc/pacman.d/hooks/startnyckel.hook \
	etc/systemd/logind.conf.d/12vt.conf \
	etc/systemd/system/getty@.service.d/l10n.conf \
	etc/systemd/system/getty@.service.d/ludvigetty.conf \
	etc/systemd/timesyncd.conf.d/ntp.se.conf \
	etc/systemd/tpcbbt.service \
	etc/tmpfiles.d/har_inte_loggats_in.conf \
	etc/udev/hwdb.d/90-libinput-x220-touchpad-fw81.hwdb \
	etc/X11/xorg.conf.d/00-tangentbord.conf \
	etc/X11/xorg.conf.d/10-skärm.conf \
	etc/X11/xorg.conf.d/50-musplatta.conf \
	etc/X11/xorg.conf.d/50-trackpoint.conf
do
	sudo cp -f "$KONF/System/$file" "/$file"
done
